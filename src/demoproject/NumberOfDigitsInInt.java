package demoproject;

public class NumberOfDigitsInInt {

	public static void main(String[] args) {
		
		long x = 10 ;
		
		int count =0;
		
		while (x != 0) {
			x=x/10;
			count++;
		}
		
		System.out.println(count);

	}

}
