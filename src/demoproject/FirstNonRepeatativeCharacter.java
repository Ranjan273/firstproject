package demoproject;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

public class FirstNonRepeatativeCharacter {

	public static void main(String[] args) {
		
		Scanner s =new Scanner(System.in);
		System.out.println("Enter the String One:");
		String s1=s.nextLine();
		
		/*
		 * System.out.println("Enter the string two :"); String s2=s.nextLine();
		 * 
		 * System.out.println("Enter the string three: "); String s3=s.nextLine();
		 */
		
		nonRepeataivechar(s1);

		//nonRepeataivechar(s2);
		
		//nonRepeataivechar(s3);
		
		char c=nonRepeataivechar(s1);
		System.out.println(c);
		

	}
	
	public static Character nonRepeataivechar(String str) {
		
		HashMap<Character, Integer> ch=new HashMap<Character,Integer>();
		char[] c=str.toCharArray();
		
		for(char ca : c) {
			if(ch.containsKey(ca)) {
				ch.put(ca, ch.get(ca)+1);
			}
			
			else {
				ch.put(ca, 1);
			}
		}
		
		System.out.println(ch);
		
		for(Entry<Character, Integer> entry:ch.entrySet()) {
			if(entry.getValue()==1) {
				return entry.getKey();
			}
		}
		return null;
	}

}
