package demoproject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class FirstnonRepeatingchar {

	public static void main(String[] args) {
		
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the string :");
		String str=s.nextLine();
		
		char c=firstnonrepeatchar(str);
		System.out.println(c);
		
		
		

	}
	
	public static  Character firstnonrepeatchar(String input) {
		
		Set<Character> repeatingchar=new HashSet<Character>();
		List<Character> nonrepeatingchar=new ArrayList<Character>();
		for(int i=0;i<input.length();i++) {
			char ch=input.charAt(i);
			
			if(repeatingchar.contains(ch))
				continue;
			if(nonrepeatingchar.contains(ch)) {
				nonrepeatingchar.remove((Character)ch);
				repeatingchar.add(ch);
			}
			else {
				nonrepeatingchar.add(ch);
			}
			
		}
		return nonrepeatingchar.get(0);
		
	}

}
