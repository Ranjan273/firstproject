package demoproject;

import java.util.HashMap;
import java.util.Scanner;

public class CountCharacterInAnArray {

	public static void main(String[] args) {
		
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the string :");
		String str=s.nextLine();
		
		System.out.println("Enter String 1 :");
		String str1=s.nextLine();
		
		System.out.println("Enter the string 2");
		String str2=s.nextLine();
		
		countcharinstring(str);
		
		countcharinstring(str1);
		
		countcharinstring(str2);
		
		//countcharinstring(null);

	}
	
	public static void countcharinstring(String input) {
		
		HashMap<Character, Integer> eachchar=new HashMap<Character,Integer>();
		
		char c[]=input.toCharArray();
		
		for(char ch : c) {
			if(eachchar.containsKey(ch)) {
				eachchar.put(ch, eachchar.get(ch)+1);
			}
			else {
				eachchar.put(ch, 1);
			}
		}
		
		System.out.println(eachchar);
		
	}

}
