package DemoTwoProject;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

public class PowerConcept {

	public static void main(String[] args) {
		 
		Scanner s =new Scanner(System.in);
		System.out.println("Enter the String :");
		String myString=s.nextLine();
		
		
	}
	
	public void nonRepeatableChar(String input) {
		HashMap<Character, Integer> hm=new HashMap<Character,Integer>();
		char ch[]=input.toCharArray();
		
		for(char c : ch) {
			
			if(hm.containsKey(c)) {
				hm.put(c, hm.get(c)+1);
			}
			
			else {
				hm.put(c, 1);
			}
		}
		
		System.out.println(hm);
		
		for(Entry<Character, Integer> entry:hm.entrySet()) {
			if(entry.getKey()==1) {
				entry.getValue();
			}
		}
		
	}

}
