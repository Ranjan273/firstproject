package basicPrograms;

public class PrimeNumber {

	public static void main(String[] args) {

        int num=28;
        
        for(int i=2;i<num/2;i++) {
        	if(num % i==0) {
        		System.out.println(num +" is not prime number");
        		break;
        	}
        	
        	else {
        		System.out.println(num +" is prime number");
        		break;
        	}
        }

	}

}
