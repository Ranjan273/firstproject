package basicPrograms;

import java.util.Scanner;

public class ArmstrongNumberUpTo {
	
	public static boolean isArmstrongNumber(int n) {
		int temp,sum=0,digits=0,last=0;
		
		temp=n;
		while(temp>0) {
			temp=temp/10;
			digits++;
		}
		temp=n;
		while(temp>0) {
			last=temp % 10;
			sum +=  (Math.pow(last, digits));
			temp=temp/10;
		}
		
		if(n==sum)
			return true;
		else
			return false;
	}
	

	public static void main(String[] args) {

          Scanner s=new Scanner(System.in);
          System.out.println("Enter the Number");
          int limit=s.nextInt();
          
          for(int i=0;i<=limit;i++) 
        	  if(isArmstrongNumber(i))
        		  System.out.print(i+" ");
	}

}
