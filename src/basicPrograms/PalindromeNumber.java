package basicPrograms;

public class PalindromeNumber {

	public static void main(String[] args) {

       int num=131;
       int sum=0;
       
       while(num >0) {
    	   int temp=num%10;
    	   sum=sum*10+temp;
    	   num=num/10;
       }
       System.out.println(sum);
       System.out.println(num);
       
       if(sum==num) {
    	   System.out.println("Number is palindrome");
       }
       
       else {
    	   System.out.println("number is not palindrome");
       }
    

	}

}
