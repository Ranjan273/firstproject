package dailyPracticeOne;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class firstnonrepeatchar {

	public static void main(String[] args) {
		
		String str="sasan";
		
		char c=nonrepeatingchar(str);
		System.out.println(c);
		

	}
	
	public static Character nonrepeatingchar(String str) {
		Set<Character> repeat=new HashSet<Character>();
		List<Character> nonrepeat =new ArrayList<Character>();
		
		for(int i=0;i<str.length();i++) {
			char c=str.charAt(i);
			
			if(repeat.contains(c)) 
				continue;
			if(nonrepeat.contains(c)) {
				nonrepeat.remove((Character)c);
				repeat.add(c);
			}
			else {
				nonrepeat.add(c);
			}
		}
		
		return nonrepeat.get(0);
	}

}
